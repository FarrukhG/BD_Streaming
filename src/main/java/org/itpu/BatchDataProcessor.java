package org.itpu;

import org.apache.spark.sql.*;

public class BatchDataProcessor {
    private SparkSession spark;

    public BatchDataProcessor(SparkSession spark) {
        this.spark = spark;
    }

    /**
     * Process batch data from local storage.
     * Reads receipt_restaurants and weather data, performs data enrichment, filtering, calculations,
     * aggregation, and stores the result in CSV format. Specific implementation details are left as comments.
     */
    public void processData() {
        // Read receipt_restaurants data from local storage
        Dataset<Row> receiptRestaurantsDF = spark.read().option("header", "true").csv("src/main/resources/receipt_restaurant/*.csv");

        // Read weather data from local storage
        Dataset<Row> weatherDF = spark.read().option("header", "true").csv("src/main/resources/weather/*.csv");
    }
}