package org.itpu;

import org.apache.spark.sql.*;

import java.util.concurrent.TimeoutException;

public class Main {

    /**
     * Main method to initialize SparkSession, call methods to process batch and streaming data, and stop the SparkSession.
     * @param args Command-line arguments.
     * @throws Exception If an error occurs during execution.
     */
    public static void main(String[] args) throws Exception {
        SparkSession spark = SparkSession.builder()
                .appName("StreamProcessingTask")
                .master("local[*]")
                .getOrCreate();

        processBatchData(spark);
        processStreamingData(spark);

        spark.stop();
    }

    /**
     * Method to process batch data.
     * @param spark The SparkSession to be used for processing.
     */
    private static void processBatchData(SparkSession spark) {
        BatchDataProcessor batchProcessor = new BatchDataProcessor(spark);
        batchProcessor.processData();
    }

    /**
     * Method to process streaming data.
     * @param spark The SparkSession to be used for processing.
     * @throws TimeoutException If the streaming query doesn't terminate within the specified time.
     */
    private static void processStreamingData(SparkSession spark) throws TimeoutException {
        StreamingDataProcessor streamingProcessor = new StreamingDataProcessor(spark);
        streamingProcessor.processData();
    }
}