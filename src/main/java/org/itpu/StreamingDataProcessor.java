package org.itpu;

import org.apache.spark.sql.*;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.sql.types.*;

import java.util.concurrent.TimeoutException;

public class StreamingDataProcessor {
    private SparkSession spark;

    public StreamingDataProcessor(SparkSession spark) {
        this.spark = spark;
    }

    /**
     * Process streaming data from the input directory.
     * Reads data with a specified schema, applies additional logic based on average temperature,
     * and writes the processed data to the output directory using Spark Streaming.
     * @throws TimeoutException If the streaming query doesn't terminate within the specified time.
     */
    public void processData() throws TimeoutException {
        // Define schema for the streaming DataFrame
        StructType schema = new StructType()
                .add("id", DataTypes.StringType)
                .add("franchise_id", DataTypes.StringType)
                .add("franchise_name", DataTypes.StringType)
                .add("restaurant_franchise_id", DataTypes.StringType)
                .add("country", DataTypes.StringType)
                .add("city", DataTypes.StringType)
                .add("lat", DataTypes.DoubleType)
                .add("lng", DataTypes.DoubleType)
                .add("receipt_id", DataTypes.StringType)
                .add("total_cost", DataTypes.DoubleType)
                .add("discount", DataTypes.DoubleType)
                .add("date_time", DataTypes.TimestampType)
                .add("avg_tmpr_c", DataTypes.DoubleType);

        // Read streaming data from input directory with the specified schema
        Dataset<Row> streamingDF = spark.readStream()
                .option("header", "true")
                .schema(schema)
                .csv("src/main/resources/input_directory");

        // Apply additional logic based on average temperature
        Dataset<Row> streamingProcessedDF = streamingDF.withColumn("promo_cold_drinks",
                functions.when(functions.col("avg_tmpr_c").leq(25.0), "FALSE").otherwise("TRUE"));

        // Write processed data to output directory using Spark Streaming
        StreamingQuery streamingQuery = streamingProcessedDF.writeStream()
                .format("csv")
                .option("path", "src/main/resources/output_streaming_directory")
                .option("checkpointLocation", "src/main/resources/checkpoint_directory")
                .start();

        try {
            streamingQuery.awaitTermination();
        } catch (StreamingQueryException e) {
            e.printStackTrace();
        }
    }
}
